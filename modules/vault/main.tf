resource "aws_security_group" "vault_sg" {
    name = "${var.environment}-vault-sg"
    description = "vault security group"

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 8200
        to_port = 8200
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    vpc_id = "${var.vpc_id}"

    tags {
      Name        = "${var.environment}-public-sg"
      Environment = "${var.environment}"
    }
}

resource "aws_instance" "vault" {
    ami = "${var.image_id}"
    vpc_security_group_ids = [ "${aws_security_group.vault_sg.id}" ]
    instance_type = "${var.instance_type}"
    key_name      = "${var.key_name}"
    associate_public_ip_address = true
    tags {
        Name = "${var.environment}-vault"
    }
    subnet_id = "${element(var.public_subnets_id, count.index)}"

    depends_on = ["aws_instance.vault"]

    connection {
      user         = "ec2-user"
      private_key  = "${file("~/.aws/a4.pem")}"
    }

    provisioner "remote-exec" {
      inline = [
        "sudo wget https://releases.hashicorp.com/vault/0.11.4/vault_0.11.4_linux_amd64.zip",
        "sudo unzip vault_0.11.4_linux_amd64.zip -d /usr/local/bin",
        "sudo chmod 777 /usr/local/etc",
        "sudo chmod 777 /usr/lib/systemd/system"
      ]
    }

    provisioner "file" {
      source = "${path.module}/vault_config/vault.cfg"
      destination = "/usr/local/etc/vault.cfg"
    }

    provisioner "file" {
      source = "${path.module}/vault_config/vault.service"
      destination = "/usr/lib/systemd/system/vault.service"
    }

    provisioner "remote-exec" {
      inline = [
        "sudo openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -subj \"/C=UK/ST=London/L=London/O=AL/OU=dev/CN=\"${aws_instance.vault.public_dns}\"/EA=vlad@al.com\" -out /usr/local/etc/vault.crt -keyout /usr/local/etc/vault.key",
        "sudo systemctl daemon-reload",
        "VAULT_ADDR=https://${aws_instance.vault.public_dns}:8200",
        "VAULT_SKIP_VERIFY=1",
        "export VAULT_SKIP_VERIFY VAULT_ADDR",
        "echo \"VAULT_ADDR=$VAULT_ADDR\" >> .bash_profile",
        "echo \"VAULT_SKIP_VERIFY=$VAULT_SKIP_VERIFY\" >> .bash_profile",
        "echo \"export VAULT_ADDR VAULT_SKIP_VERIFY\" >> .bash_profile",
        "sudo systemctl enable vault.service",
        "sudo systemctl start vault.service",
        "/usr/local/bin/vault operator init > /usr/local/etc/keys"
      ]
    }

    provisioner "local-exec" {
        command = "scp -i ~/.aws/a4.pem -o \"StrictHostKeyChecking no\" -r ec2-user@${aws_instance.vault.public_ip}:/usr/local/etc/keys ${path.module}/vault_keys"
    }
}

resource "aws_route53_record" "vault" {
      zone_id = "${var.public_zone_id}"
      name = "a4-vault"
      type = "CNAME"
      ttl = "30"
      records = ["${aws_instance.vault.public_dns}"]
}

data "template_file" "unlock" {
  template = "${file("${path.module}/templates/unlock.tpl")}"

  vars {
    vault_ip = "${aws_instance.vault.public_ip}"
    path     = "${path.module}"
  }
}

resource "local_file" "unlock" {
    content     = "${data.template_file.unlock.rendered}"
    filename = "${path.root}/vault_actions/unlock.sh"
}

data "template_file" "status" {
  template = "${file("${path.module}/templates/status.tpl")}"

  vars {
    vault_ip = "${aws_instance.vault.public_ip}"
  }
}

resource "local_file" "status" {
    content     = "${data.template_file.status.rendered}"
    filename = "${path.root}/vault_actions/status.sh"
}

data "template_file" "seal" {
  template = "${file("${path.module}/templates/seal.tpl")}"

  vars {
    vault_ip = "${aws_instance.vault.public_ip}"
    path     = "${path.module}"
  }
}

resource "local_file" "seal" {
    content     = "${data.template_file.seal.rendered}"
    filename = "${path.root}/vault_actions/seal.sh"
}

data "template_file" "write" {
  template = "${file("${path.module}/templates/write.tpl")}"

  vars {
    vault_ip = "${aws_instance.vault.public_ip}"
    path     = "${path.module}"
  }
}

resource "local_file" "write" {
    content     = "${data.template_file.write.rendered}"
    filename = "${path.root}/vault_actions/write.sh"
}

# resource "null_resource" "vault" {
#   provisioner "local-exec" {
#       command = "${path.root}/vault_actions/unlock.sh && ${path.root}/vault_actions/write.sh && ${path.root}/vault_actions/seal.sh"
#   }
# }
