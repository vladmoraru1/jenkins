#!/bin/bash

init_token=$(grep "Initial Root Token" ${path}/vault_keys/keys | cut -d ":" -f2 | xargs)

curl -k --header "X-Vault-Token:$init_token" --request PUT https://${vault_ip}:8200/v1/sys/seal
